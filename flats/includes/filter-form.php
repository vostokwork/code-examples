<form class="modal-form__filter js-modalApartmentForm js-filterCheckboxs">
    <?php
    $checkedProjects = [];
    foreach ($arResult['CITY_PROJECTS'] as $project) {
        if ($project['CHECKED']) {
            $checkedProjects[] = $project['CODE'];
        }
    }
    ?>
    <input name="filter[PROJECT.UF_CODE]" class="dropbox-checkboxs__value js-apartmentCheckboxsValue" type="hidden" value="<?=implode(';', $checkedProjects)?>">
    <div class="modal-filter__flex">
        <div class="modal-filter__flex-item">
            <h3 class="apartment-title">Количество комнат</h3>
            <div class="buttons-list">
                <?php
                foreach ($arResult['FLATS_FILTERS']['PLAN.UF_TYPE'] as $value) {
                    ?>
                    <label class="button-checkbox">
                        <input
                            type="checkbox"
                            name="filter[PLAN.UF_TYPE][]"
                            value="<?=$value['VALUE']?>"
                            <?=($value['CHECKED'] ? 'checked' : '')?>
                        >
                        <span><?=$value['NAME']?></span>
                    </label>
                    <?php
                }
                ?>
            </div>
        </div>
        <div class="modal-filter__flex-item">
            <div class="modal-grid">
                <div class="modal-grid__col">
                    <h3 class="apartment-title">Стоимость,
                        <svg class="icon icon-ruble ">
                            <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                        </svg>
                    </h3>
                    <div class="apartments-filter__range">
                        <div class="range-slider">
                            <input
                                class="range-slider js-rangeSlider js-rangeBinding"
                                type="text"
                                data-min="<?=$arResult['FLATS_FILTERS']['UF_TOTAL_PRICE']['MIN']?>"
                                data-max="<?=$arResult['FLATS_FILTERS']['UF_TOTAL_PRICE']['MAX']?>"
                                data-from="<?=$arResult['FLATS_FILTERS']['UF_TOTAL_PRICE']['DEFAULT_MIN']?>"
                                data-from-default="<?=$arResult['FLATS_FILTERS']['UF_TOTAL_PRICE']['DEFAULT_MIN']?>"
                                data-to="<?=$arResult['FLATS_FILTERS']['UF_TOTAL_PRICE']['DEFAULT_MAX']?>"
                                data-to-default="<?=$arResult['FLATS_FILTERS']['UF_TOTAL_PRICE']['DEFAULT_MAX']?>"
                                data-slider="price"
                                name="filter[UF_TOTAL_PRICE]"
                            >
                        </div>
                    </div>
                </div>
                <div class="modal-grid__col">
                    <h3 class="apartment-title">Площадь, м<sup>2</sup></h3>
                    <div class="apartments-filter__range">
                        <div class="range-slider">
                            <input
                                class="range-slider js-rangeSlider js-rangeBinding"
                                type="text"
                                data-min="<?=$arResult['FLATS_FILTERS']['PLAN.UF_TOTAL_AREA']['MIN']?>"
                                data-max="<?=$arResult['FLATS_FILTERS']['PLAN.UF_TOTAL_AREA']['MAX']?>"
                                data-from="<?=$arResult['FLATS_FILTERS']['PLAN.UF_TOTAL_AREA']['DEFAULT_MIN']?>"
                                data-from-default="<?=$arResult['FLATS_FILTERS']['PLAN.UF_TOTAL_AREA']['DEFAULT_MIN']?>"
                                data-to="<?=$arResult['FLATS_FILTERS']['PLAN.UF_TOTAL_AREA']['DEFAULT_MAX']?>"
                                data-to-default="<?=$arResult['FLATS_FILTERS']['PLAN.UF_TOTAL_AREA']['DEFAULT_MAX']?>"
                                data-slider="area"
                                name="filter[PLAN.UF_TOTAL_AREA]"
                            >
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal-grid style_column_2">
        <div class="modal-grid__col style_flex">
            <?php if (!empty($arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']) && $arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['MIN'] !== $arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['MAX']): ?>
                <div class="modal-grid__col-item">
                    <h3 class="apartment-title">Год сдачи</h3>
                    <div class="apartments-filter__range">
                        <div class="range-slider">
                            <input
                                class="js-rangeFilterYear range-slider js-rangeSlider js-rangeBinding"
                                type="text"
                                data-min="<?=$arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['MIN']?>"
                                data-max="<?=$arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['MAX']?>"
                                data-from="<?=$arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['DEFAULT_MIN']?>"
                                data-from-default="<?=$arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['DEFAULT_MIN']?>"
                                data-to="<?=$arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['DEFAULT_MAX']?>"
                                data-to-default="<?=$arResult['FLATS_FILTERS']['PROJECT.UF_COMPLETION']['DEFAULT_MAX']?>"
                                data-slider="year"
                                name="filter[PROJECT.UF_COMPLETION]"
                            >
                        </div>
                    </div>
                </div>
            <?php endif; ?>
            <?php if (!empty($arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER'])): ?>
                <div class="modal-grid__col-item">
                    <h3 class="apartment-title">Этаж</h3>
                    <div class="apartments-filter__range">
                        <div class="range-slider">
                            <input
                                class="range-slider js-rangeFloor js-rangeSlider js-modalFilterRange js-rangeBinding"
                                type="text"
                                data-min="<?=$arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['MIN']?>"
                                data-max="<?=$arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['MAX']?>"
                                data-from="<?=$arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['DEFAULT_MIN']?>"
                                data-from-default="<?=$arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['DEFAULT_MIN']?>"
                                data-to="<?=$arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['DEFAULT_MAX']?>"
                                data-to-default="<?=$arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['DEFAULT_MAX']?>"
                                name="filter[FLOOR.UF_NUMBER]"
                                data-slider="floor"
                            >
                        </div>
                    </div>
                </div>
                <?php
                $displayFirstFloorCheckbox = (int) $arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['MIN'] === 1;
                $displayLastFloorCheckbox = (int) $arResult['FLATS_FILTERS']['FLOOR.UF_NUMBER']['MAX'] === (int) $arResult['FLATS_FILTERS']['FLOOR_NUMBER_MAX']['VALUE'];
                ?>
                <?php if ($displayFirstFloorCheckbox || $displayLastFloorCheckbox): ?>
                    <div class="modal-grid__col-item">
                        <div class="apartments-filter__checkbox js-filterModalCheckboxs">
                            <?php if ($displayFirstFloorCheckbox): ?>
                                <label class="checkbox js-notFirst">
                                    <input
                                        type="checkbox"
                                        name="filter[>FLOOR.UF_NUMBER]"
                                        value="1"
                                        <?=((int) $arResult['FILTER']['>FLOOR.UF_NUMBER'] === 1 ? 'checked' : '')?>
                                    >
                                    <div class="checkbox__title">Не первый</div>
                                </label>
                            <?php endif; ?>
                            <?php if ($displayLastFloorCheckbox): ?>
                                <label class="checkbox js-notLast">
                                    <input
                                        type="checkbox"
                                        name="filter[<FLOOR.UF_NUMBER]"
                                        value="<?=$arResult['FLATS_FILTERS']['FLOOR_NUMBER_MAX']['VALUE']?>"
                                        <?=((int) $arResult['FILTER']['<FLOOR.UF_NUMBER'] === (int) $arResult['FLATS_FILTERS']['FLOOR_NUMBER_MAX']['VALUE'] ? 'checked' : '')?>
                                    >
                                    <div class="checkbox__title">Не последний</div>
                                </label>
                            <?php endif; ?>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>
        </div>
        <?php if (count($arResult['FLATS_FILTERS']['UF_POSITIVE_OUTSIDE']) === 1 || count($arResult['FLATS_FILTERS']['PARKING_AND_PANTRY']) === 1): ?>
            <div class="modal-grid__col">
                <h3 class="apartment-title">Дополнительно</h3>
                <div class="buttons-list">
                    <?php if (count($arResult['FLATS_FILTERS']['UF_POSITIVE_OUTSIDE']) === 1): ?>
                        <?php foreach ($arResult['FLATS_FILTERS']['UF_POSITIVE_OUTSIDE'] as $value): ?>
                            <label class="button-checkbox">
                                <input
                                    type="checkbox"
                                    name="filter[UF_POSITIVE_OUTSIDE][]"
                                    value="<?=$value['VALUE']?>"
                                    <?=($value['CHECKED'] ? 'checked' : '')?>
                                >
                                <span><?=$value['NAME']?></span>
                            </label>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if (count($arResult['FLATS_FILTERS']['PARKING_AND_PANTRY']) === 1): ?>
                        <?php foreach($arResult['FLATS_FILTERS']['PARKING_AND_PANTRY'] as $fieldName => $value): ?>
                            <label class="button-checkbox">
                                <input
                                    type="checkbox"
                                    name="filter[<?=$fieldName?>]"
                                    value="<?=$value['VALUE']?>"
                                    <?=($value['CHECKED'] ? 'checked' : '')?>
                                >
                                <span><?=$value['NAME']?></span>
                            </label>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </div>
            </div>
        <?php endif; ?>
    </div>
    <?php if (count($arResult['FLATS_FILTERS']['UF_POSITIVE_OUTSIDE']) > 1) { ?>
        <div class="apartment-fieldset">
            <h3 class="apartment-title">Окна и балкон</h3>
            <div class="buttons-list">
                <?php
                foreach ($arResult['FLATS_FILTERS']['UF_POSITIVE_OUTSIDE'] as $value) {
                    ?>
                    <label class="button-checkbox">
                        <input
                            type="checkbox"
                            name="filter[UF_POSITIVE_OUTSIDE][]"
                            value="<?=$value['VALUE']?>"
                            <?=($value['CHECKED'] ? 'checked' : '')?>
                        >
                        <span><?=$value['NAME']?></span>
                    </label>
                    <?php
                }
                ?>
            </div>
        </div>
    <?php } ?>
    <div class="modal-filter__flex style_2">
        <?php
        if (!empty($arResult['FLATS_FILTERS']['PLAN.UF_RESIDENTS'])) {
            ?>
            <div class="modal-filter__flex-item">
                <div class="apartment-fieldset">
                    <h3 class="apartment-title">Состав жителей</h3>
                    <div class="buttons-list">
                        <?php
                        foreach ($arResult['FLATS_FILTERS']['PLAN.UF_RESIDENTS'] as $value) {
                            ?>
                            <label class="button-checkbox">
                                <input
                                    type="checkbox"
                                    name="filter[PLAN.UF_RESIDENTS][]"
                                    value="<?=$value['VALUE']?>"
                                    <?=($value['CHECKED'] ? 'checked' : '')?>
                                >
                                <span><?=$value['NAME']?></span>
                            </label>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        <?php
        if (count($arResult['FLATS_FILTERS']['PARKING_AND_PANTRY']) > 1) {
            ?>
            <div class="modal-filter__flex-item">
                <div class="apartment-fieldset">
                    <h3 class="apartment-title">Планирую покупку или аренду</h3>
                    <div class="buttons-list">
                        <?php foreach($arResult['FLATS_FILTERS']['PARKING_AND_PANTRY'] as $fieldName => $value): ?>
                            <label class="button-checkbox">
                                <input
                                    type="checkbox"
                                    name="filter[<?=$fieldName?>]"
                                    value="<?=$value['VALUE']?>"
                                    <?=($value['CHECKED'] ? 'checked' : '')?>
                                >
                                <span><?=$value['NAME']?></span>
                            </label>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
    <div class="modal-form__filter-button">
        <button class="js-button-filter-submit button style_blue <?=(empty($arResult['FLATS_COUNT']) ? 'hidden' : '')?>">
            <div class="button__text js-modalClose">
                Показать
                <span class="js-count-pluralize">
                    <?php if ($arParams['VIEW'] === 'list'): ?>
                        <?=$this->getComponent()->getFlatsPluralizePhrase($arResult['FLATS_COUNT'])?>
                    <?php else: ?>
                        <?=DS\Development_Layout\Helpers\Pluralize::getByType($arResult['PLANS_COUNT'], 'plan')?>
                    <?php endif; ?>
                </span>
            </div>
        </button>
        <button class="js-button-filter-empty button style_blue disabled <?=(empty($arResult['FLATS_COUNT']) ? '' : 'hidden')?>">
            <div class="button__text">
                По выбранным значениям нет квартир в продаже
            </div>
        </button>
        <a class="filter-reset js-resetFilter is-hide" href="javascript:;">
            <div class="filter-reset__text">очистить фильтр</div>
            <div class="filter-reset__icon">
                <svg class="icon icon-close ">
                    <use xlink:href="images/sprites.svg#close"></use>
                </svg>
            </div>
        </a>
    </div>
</form>
