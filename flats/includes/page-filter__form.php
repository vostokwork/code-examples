<div class="page-filter__form">
    <div class="page-filter__form-wrap filter-dropbox js-filterDropbox <?=($arResult['FLATS_COUNT'] === 0 && $arResult['PLANS_COUNT'] === 0 ? 'hidden' : '')?>">
        <div class="container hidden-xs">
        <?php
        require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/filter-form.php';
        ?>
        </div>
    </div>
    <div class="page-filter__buttons-wrap">
        <div class="container">
            <div class="page-filter__buttons">
                <div class="page-filter__button button_tablet">
                    <a class="js-modalLink link link_text_uppercase" href="javascript:;" data-mfp-src="#modalFilterApartment">
                        <span class="link__title">Расширенный фильтр</span>
                        <span class="link__icon">
                            <svg class="icon icon-plus ">
                                <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#plus"></use>
                            </svg>
                        </span>
                    </a>
                </div>
                <div class="page-filter__button button_desktop">
                    <a class="js-openFilterDropbox link link_text_uppercase" href="javascript:;">
                        <div class="link__title">
                            <span class="link__title-dropbox">Расширенный фильтр</span>
                            <span class="link__title-dropbox">свернуть фильтр</span>
                        </div>
                        <span class="link__icon plus"></span>
                    </a>
                </div>
                <div class="page-filter__button hidden-xs">
                    <a class="filter-reset js-resetFilter is-hide" href="javascript:;">
                        <div class="filter-reset__text">очистить фильтр</div>
                        <div class="filter-reset__icon">
                            <svg class="icon icon-close ">
                                <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#close"></use>
                            </svg>
                        </div>
                    </a>
                </div>
                <div class="page-header-map">
                    <a class="map-point" href="/projects/map/<?=(!empty($arResult['CITY']) && $arResult['CITY'] !== 'all' ? '?city=' . $arResult['CITY'] : '')?>">
                        <div class="map-point__title">
                            <div class="link">
                                <span class="link__text">Проекты на карте</span>
                                <span class="link__icon"><img src="<?= MARKUP_PATH ?>/images/map_point.svg"></span>
                            </div>
                        </div>
                        <div class="map-point__back" style="background-image: url('<?= MARKUP_PATH ?>/images/map_point_back.jpg')">
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>
