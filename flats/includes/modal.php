<div class="modal modal-filter__apartment mfp-hide bg_grey modal-has-header" id="modalFilterApartment">
    <div class="container">
        <div class="modal-box">
            <div class="modal-header">
                <h3 class="modal-header__title">Расширенный фильтр</h3>
                <a class="modal-close js-modalClose" href="javascript:;">
                    <svg class="icon icon-close "><use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#close"></use></svg>
                </a>
            </div>
            <?php
            require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/filter-form.php';
            ?>
        </div>
    </div>
</div>
