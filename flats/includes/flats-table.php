<div class="js-page-table page-table <?=(empty($arResult['FLATS']) ? 'hidden' : '')?>">
    <div class="page-table__header">
        <div class="page-title">
            <h2 class="page-title__value">
                Подобрано
                <span class="js-count-pluralize">
                    <?=$this->getComponent()->getFlatsPluralizePhrase($arResult['FLATS_COUNT'])?>
                </span>
            </h2>
            <div class="page-header-link">
                <a class="link js-switch-link" href="/flats/plan/<?=(!empty($arResult['FILTER']) ? '?' . http_build_query(['filter' => $arResult['FILTER']]) : '')?>">
                    <span class="link__text">Объединить по планировкам</span>
                    <span class="link__number js-switchCount"><?=$arResult['PLANS_COUNT']?></span>
                </a>
            </div>
        </div>
    </div>
    <div class="table apartment-table">
        <div class="apartment-table__wrap">
            <table>
                <thead class="hidden-xs">
                    <tr>
                        <th>Планировка</th>
                        <th>
                            <div data-order-field="PLAN.UF_ROOMS_COUNT" data-order-direction="ASC" class="sort-header sort-up order-hidden pointer">
                                <span class="sort-header__title">Комнаты</span>
                                <svg class="icon icon-dropdown-arrow">
                                    <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use>
                                </svg>
                            </div>
                        </th>
                        <th>
                            <div data-order-field="PLAN.UF_TOTAL_AREA" data-order-direction="ASC" class="sort-header sort-up order-hidden pointer">
                                <span class="sort-header__title">Площадь, м<sup>2</sup></span>
                                <svg class="icon icon-dropdown-arrow">
                                    <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use>
                                </svg>
                            </div>
                        </th>
                        <th>ЖК</th>
                        <th>Корпус/Секция</th>
                        <th>Этаж</th>
                        <th>
                            <div data-order-field="FLAT.UF_TOTAL_PRICE" data-order-direction="ASC" class="sort-header sort-up pointer">
                                <span class="sort-header__title">Стоимость,
                                    <svg class="icon icon-ruble ">
                                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                                    </svg>
                                </span>
                                <svg class="icon icon-dropdown-arrow">
                                    <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use>
                                </svg>
                            </div>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody id="flats_list_table">
                    <?php foreach ($arResult['FLATS'] as $index => $flat): ?>
                        <tr class="js-link" data-url="<?=$flat['DETAIL_URL']?>">
                            <td><img src="<?=$flat['IMAGE']['src']?>" width="<?=$flat['IMAGE']['width']?>" height="<?=$flat['IMAGE']['height']?>" alt=""></td>
                            <td><?=$flat['TYPE']?></td>
                            <td><?=$flat['TOTAL_AREA']?></td>
                            <td>
                                <div class="mobile-label">ЖК</div>
                                <span><?=$flat['PROJECT']?></span>
                            </td>
                            <td>
                                <div class="mobile-label">Корпус/Секция</div>
                                <span><?=$flat['SECTION']?></span>
                            </td>
                            <td>
                                <div class="mobile-label">Этаж</div>
                                <span><?=$flat['FLOOR']?></span>
                            </td>
                            <td>
                                <div class="mobile-label">
                                    Стоимость,
                                    <svg class="icon icon-ruble ">
                                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use>
                                    </svg>
                                </div>
                                <span><?=$flat['TOTAL_PRICE']?></span>
                            </td>
                            <td>
                                <a class="favorite-button js-favorite-add <?=($flat['IS_FAVORITE'])?'in_favourite':''?>"
                                    href="javascript:;"
                                    data-id="<?=$flat['ID']?>"
                                    data-type="flat">
                                    <svg class="icon icon-fav ">
                                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#fav"></use>
                                    </svg>
                                </a>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
<div class="show-more">
    <a class="link link_text_uppercase js-load-more <?=($arResult['CAN_MORE'] ? '' : 'hidden')?>" href="#">
        <span class="link__title">загрузить еще</span>
        <span class="link__icon">
            <svg class="icon icon-plus "><use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#plus"></use></svg>
        </span>
    </a>
</div>
<div class="no-result js-no-result <?=($arResult['FLATS_COUNT'] ? 'hidden' : '')?>">
    <div class="no-result__image"><img src="<?= MARKUP_PATH ?>/images/no-result.svg" alt="Нет результатов"></div>
    <div class="no-result__text">Не найдено подходящих вариантов. Попробуйте изменить <br> значения фильтров</div>
</div>
