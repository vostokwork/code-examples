<?php
use DS\Development_Layout\Helpers\Project as ProjectHelper;
?>
<div class="js-page-table page-table <?=($arResult['PLANS_COUNT'] ? '' : 'hidden')?>">
    <div class="page-table__header">
        <div class="page-title">
            <h2 class="page-title__value">
                <span class="js-count-pluralize">
                    <?=DS\Development_Layout\Helpers\Pluralize::getByType($arResult['PLANS_COUNT'], 'plan')?>
                </span>
            </h2>
            <div class="page-header-link">
                <a class="link js-switch-link" href="/flats/list/<?=(!empty($arResult['FILTER']) ? '?' . http_build_query(['filter' => $arResult['FILTER']]) : '')?>">
                    <span class="link__text">
                        Показать все
                        <?php
                        switch ($arResult['PROJECT_LIVE_TYPE']) {
                            case ProjectHelper::FLAT_TYPE:
                                echo 'квартиры';
                                break;
                            case ProjectHelper::APARTMENT_TYPE:
                                echo 'апартаменты';
                                break;
                            case ProjectHelper::FLAT_APARTMENT_TYPE:
                                echo 'варианты';
                                break;
                            default:
                                echo 'квартиры';
                        }
                        ?>
                    </span>
                    <span class="link__number js-switchCount"><?=$arResult['FLATS_COUNT']?></span>
                </a>
                <div class="page-table__select">
                    <div class="form-field">
                        <div class="form-label">Сортировать по</div>
                        <div class="form-control">
                            <select class="form-select js-select2Single js-sortSelect" data-placeholder="Выбрать" name="order">
                                <option value="FLAT_TOTAL_PRICE_MIN" selected>Стоимости</option>
                                <option value="PLAN.UF_ROOMS_COUNT">Количеству комнат</option>
                                <option value="PLAN.UF_TOTAL_AREA">Площади</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="project-grid plan-cards__grid js-grid js-plans-table" id="flats_list_grid" data-page-size="<?=$arResult['PLANS_COUNT']?>">
        <?php foreach ($arResult['PLANS'] as $index => $plan): ?>
            <div class="project-grid__col js-modalHeight" data-plan-id="<?=$plan['ID']?>">
                <div class="plan-card">
                    <div data-id="<?=$plan['ID']?>" data-type="plan" class="plan-card__icon js-favorite-add <?=($plan['IS_FAVORITE'] ? 'in_favourite' : '')?>">
                        <svg class="icon icon-fav "><use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#fav"></use></svg>
                    </div>
                    <a class="plan-card__image js-modalCardlLink" href="javascript:;" data-modal-src="#modalCard">
                        <img src="<?=$plan['IMAGE']['src']?>" alt="Картинка планировки">
                    </a>
                    <div class="plan-card__description">
                        <div class="plan-card__subtitle"><?=($plan['CAPTION'] ?: '&nbsp;')?></div>
                        <a  class="plan-card__title js-modalCardlLink" href="javascript:;" data-modal-src="#modalCard">
                            <?=$plan['TYPE']?>
                        </a>
                        <div class="plan-card__area"><?=$plan['AREA']?> м<sup>2</sup></div>
                    </div>
                    <div class="plan-card__list">
                        <?php for ($index = 0; $index <= 2; $index++): ?>
                            <?php
                            if (empty($plan['PROJECTS'][$index])) {
                                break;
                            }
                            ?>
                            <div class="plan-card__elem">
                                <div class="plan-card__elem-item">ЖК «<?=$plan['PROJECTS'][$index]['PROJECT_NAME']?>»</div>
                                <div class="plan-card__elem-numbers"><?=$plan['PROJECTS'][$index]['FLATS_COUNT']?></div>
                            </div>
                        <?php endfor; ?>
                    </div>
                    <div class="plan-card__text"><?=$plan['MORE_TEXT']?></div>
                    <div class="plan-card__price">от <?=$plan['PRICE']?> ₽</div>
                </div>
            </div>
        <?php endforeach; ?>
        <div class="modal-card js-modalCard" id="modalCard">
            <div class="modal-card__box"><a class="modal-card__close js-modalCardClose" href="javascript:;">
                    <svg class="icon icon-close ">
                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#close"></use>
                    </svg></a>
                <div class="modal-card__aside">
                    <div class="modal-card__aside-header">
                        <div class="modal-card__title">2-комнатная</div>
                        <div class="modal-card__subtitle">63,5</div>
                    </div>
                    <div class="modal-card__image"></div>
                </div>
                <form class="modal-card__content">
                    <div class="modal-card__content-head">
                        <div class="modal-card__content-text">11 квартир в этой планировке</div>
                        <div class="modal-card__content-select visible-xs">
                            <div class="form-field">
                                <div class="form-label">Сортировать по</div>
                                <div class="form-control">
                                    <select class="form-select js-select2Single" data-placeholder="Выбрать" name="type">
                                        <option value="0" selected>Стоимости</option>
                                        <option value="2">Площади</option>
                                        <option value="3">Этажу</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-card__table js-scroll">
                        <div class="table apartment-table">
                            <table>
                                <thead class="hidden-xs">
                                    <tr>
                                        <th>ЖК</th>
                                        <th>Корпус/Секция</th>
                                        <th>Этаж</th>
                                        <th>
                                            <div class="sort-header sort-up pointer js-modal-card-sort">
                                                <span class="sort-header__title">Стоимость, <svg class="icon icon-ruble "><use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#ruble"></use></svg></span>
                                                <svg class="icon icon-dropdown-arrow "><use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#dropdown-arrow"></use></svg>
                                            </div>
                                        </th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody></tbody>
                            </table>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<div class="show-more">
    <a class="link link_text_uppercase js-load-more <?=($arResult['CAN_MORE'] ? '' : 'hidden')?>" href="#">
        <span class="link__title">загрузить еще</span>
        <span class="link__icon">
            <svg class="icon icon-plus "><use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#plus"></use></svg>
        </span>
    </a>
</div>
<div class="no-result js-no-result <?=($arResult['PLANS_COUNT'] ? 'hidden' : '')?>">
    <div class="no-result__image"><img src="<?= MARKUP_PATH ?>/images/no-result.svg" alt="Нет результатов"></div>
    <div class="no-result__text">Не найдено подходящих планировок. Попробуйте изменить <br> значения фильтров</div>
</div>
