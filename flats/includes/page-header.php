
<div class="page-header">
    <div class="container">
        <div class="page-header__top">
            <?$APPLICATION->IncludeComponent(
                "bitrix:breadcrumb",
                ".default",
                Array(
                    "START_FROM" => "0",
                    "PATH" => "",
                    "SITE_ID" => ""
                ),
                $component
            );?>
        </div>
        <div class="page-header__main">
            <div class="page-title">
                <div class="page-title__value">Выбор квартиры</div>
                <div class="page-title__city">
                    <div class="city-toggle js-cityToggle">
                        <div class="city-toggle__control">
                            <span class="city-toggle__title"><?=$arResult['CITIES'][$arResult['CITY']]['NAME']?></span>
                            <svg class="icon icon-droplist ">
                                <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#droplist"></use>
                            </svg>
                        </div>
                    </div>
                    <div class="city-dropdown js-cityDropdown">
                        <div class="city-dropdown__list js-cityDropdownList">
                            <?php foreach ($arResult['CITIES'] as $city): ?>
                                <?php if ($city['CODE'] === $arResult['CITY']): ?>
                                    <div class="city-dropdown__title"><?=$city['NAME']?></div>
                                <?php else: ?>
                                    <div class="city-dropdown__elem">
                                        <a class="city-dropdown__link js-SelectCity" data-city="<?=$city['CODE']?>" href=""><?=$city['NAME']?></a>
                                    </div>
                                <?php endif; ?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php
        $checkedProjectsCount = count(array_filter(array_column($arResult['CITY_PROJECTS'], 'CHECKED')));
        $isAllProjectsChecked = $checkedProjectsCount === count($arResult['CITY_PROJECTS']) || $checkedProjectsCount === 0;
        ?>
        <div class="page-filter__select">
            <div class="dropbox-select__wrap js-dropboxWrap <?=($isAllProjectsChecked ? 'show-all' : '')?>">
                <div class="dropbox-select__body">
                    <div class="dropbox-select__all">
                        <div class="dropbox-select__elem js-dropboxElem">
                            <div class="dropbox-select__text">Все проекты</div>
                        </div>
                    </div>
                    <div class="dropbox-select">
                        <div class="dropbox-select__elem js-dropboxElem">
                            <div class="dropbox-select__text"><?=($checkedProjectsCount === 1 ? 'Выбран' : 'Выбрано')?> <?=$checkedProjectsCount?> ЖК</div>
                            <a href="javascript:;" class="dropbox-select__close js-dropboxClose">
                                <svg xmlns="http://www.w3.org/2000/svg" width="14" height="14" viewBox="0 0 14 14"><g><g><path d="M1.707.293A1 1 0 0 0 .293 1.707L5.586 7 .293 12.293a1 1 0 1 0 1.414 1.414L7 8.414l5.293 5.293a1 1 0 0 0 1.414-1.414L8.414 7l5.293-5.293A1 1 0 0 0 12.293.293L7 5.586z"></path></g></g></svg>
                            </a>
                        </div>
                    </div>
                </div>
                <a class="dropbox-select__button js-openDropboxPopup" href="javascript:;">
                    <svg class="icon icon-droplist ">
                        <use xlink:href="<?= MARKUP_PATH ?>/images/sprites.svg#droplist"></use>
                    </svg>
                </a>
                <div class="dropbox-select__popup js-dropboxPopup">
                    <div class="dropbox-select__popup-search">
                        <div class="dropbox-container">
                            <input class="input js-dropboxSearch" type="text" name="dropbox-search" placeholder="Начните вводить название">
                        </div>
                    </div>
                    <div class="dropbox-select__popup-checkbox">
                        <div class="dropbox-container">
                            <label class="checkbox">
                                <input class="js-dropboxCheckboxAll" type="checkbox" name="all" <?=($isAllProjectsChecked ? 'checked' : '')?>>
                                <div class="checkbox__title">Все проекты</div>
                            </label>
                        </div>
                    </div>
                    <div class="dropbox-select__popup-list js-scroll js-dropboxPopupElems">
                        <?php foreach ($arResult['CITY_PROJECTS'] as $project): ?>
                        <div class="dropbox-select__popup-elem">
                            <label class="checkbox">
                                <input type="checkbox" name="project" value="<?=$project['CODE']?>" <?=($project['CHECKED'] ? 'checked' : '')?>>
                                <div class="checkbox__title"><?=$project['NAME']?></div>
                            </label>
                        </div>
                        <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
