<?php

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}

use Bitrix\Main\Loader;
use Bitrix\Main\Web\Uri;
use Bitrix\Main\Entity\Query as BitrixQuery;
use Bitrix\Main\Web\Json;
use Bitrix\Main\Type\Date as BitrixDate;
use Bitrix\Highloadblock\HighloadBlockTable;

use DS\Development_Layout\Structure\Query;
use DS\Development_Layout\Structure\Entities;
use DS\Development_Layout\Structure\Helpers;
use DS\Development_Layout\Structure\Highload;
use DS\Development_Layout\Helpers\Pluralize;
use DS\Development_Layout\Helpers\Project as ProjectHelper;
use DS\Development_Layout\Helpers\Plan as PlanHelper;

use DS\Helpers\CityHelper;

use morphos\Russian;

if (!Loader::includeModule('ds.development_layout')) {
    ShowError('Не подключен модуль ds.development_layout');

    return;
}

class FlatsComponent extends CBitrixComponent
{
    const DEFAULT_CITY_CODE = 'all';
    const DEFAULT_LIMIT = 40;

    const PLAN_TYPES = [
        'studio' => 'Студия',
        'one_room' => '1-комнатная',
        'one_bedroom' => '2-комнатная',
        '2_euro' => '2-евро',
        'three_bedroom' => '3-комнатная',
        '3_euro' => '3-евро',
        'four_more' => '4+ комнат',
    ];

    public function onPrepareComponentParams($params)
    {
        return array_merge([
            'CACHE_TIME' => (int) $params['CACHE_TIME'] ?: 3600,
            'CACHE_TYPE' => $params['CACHE_TYPE'] ?: 'A',
        ], $params);
    }

    public function executeComponent()
    {
        $this->processLogic();

        $this->setCities();
        $this->setFlatsFilters();

        $this->IncludeComponentTemplate();
    }

    public function processLogic()
    {
        $this->setView();
        $this->setCity();
        $this->setFilter();
        $this->setOrder();
        $this->setOffset();
        $this->setLimit();
        $this->setCityProjects();
        $this->setProjectsLiveType();

        if (!empty($this->request->getQuery('ajaxAction'))) {
            $this->processAjaxAction();

            return;
        }

        if ($this->arResult['VIEW'] === 'list') {
            $this->setFlats();
        } elseif ($this->arResult['VIEW'] === 'plan') {
            $this->setPlans();
        }

        $this->setFlatsCount();
        $this->setPlansCount();
    }

    public function setView()
    {
        $this->arResult['VIEW'] = $this->request->getQuery('view') ?: $this->arParams['VIEW'];
    }

    public function setCity()
    {
        $this->arResult['CITY'] = strtolower($this->request->getQuery('city') ?: (CityHelper::getCity() ?: self::DEFAULT_CITY_CODE));
    }

    public function setFilter()
    {
        $this->arResult['FILTER'] = [];

        $filter = $this->request->getQuery('filter');

        foreach ($filter as $field => $value) {
            $newField = $field;
            $newValue = $value;

            if (is_string($value) && strpos($value, ';') !== false) {
                if ($field !== 'PROJECT.UF_CODE') {
                    $newField = '><' . $field;
                }

                $newValue = explode(';', $value);

                if ($field === 'PROJECT.UF_COMPLETION') {
                    $newValue[0] = '01.01.' . $newValue[0];
                    $newValue[1] = '31.12.' . $newValue[1];
                }
            }

            $this->arResult['FILTER'][$newField] = $newValue;
        }

        if (!empty($this->arResult['FILTER']['SUM_PARKING_COUNT'])) {
            $this->arResult['FILTER'] = array_merge($this->arResult['FILTER'], [
                [
                    'LOGIC' => 'OR',
                    ['>PROJECT.UF_PARKING_BUY' => 0],
                    ['>PROJECT.UF_PARKING_RENT' => 0],
                ],
            ]);

            unset($this->arResult['FILTER']['SUM_PARKING_COUNT']);
        }

        if (!empty($this->arResult['FILTER']['SUM_PANTRY_COUNT'])) {
            $this->arResult['FILTER'] = array_merge($this->arResult['FILTER'], [
                [
                    'LOGIC' => 'OR',
                    ['>PROJECT.UF_PANTRY_BUY' => 0],
                    ['>PROJECT.UF_PANTRY_RENT' => 0],
                ],
            ]);

            unset($this->arResult['FILTER']['SUM_PANTRY_COUNT']);
        }
    }

    public function setOrder()
    {
        $this->arResult['ORDER'] = $this->request->getQuery('order') ?: [];

        switch ($this->arResult['VIEW']) {
            case 'list':
                if (empty($this->arResult['ORDER'])) {
                    $this->arResult['ORDER'] = ['FLAT.UF_TOTAL_PRICE' => 'ASC'];
                }

                break;
            case 'plan':
                if (empty($this->arResult['ORDER'])) {
                    $this->arResult['ORDER'] = ['FLAT_TOTAL_PRICE_MIN' => 'ASC'];
                }

                break;
        }

        $this->arResult['ORDER'] = array_merge($this->arResult['ORDER'],
            ['PLAN.UF_TYPE' => key($this->arResult['ORDER']) === 'PLAN.UF_ROOMS_COUNT' ? reset($this->arResult['ORDER']) : 'ASC']
        );
    }

    public function setOffset()
    {
        $this->arResult['OFFSET'] = $this->request->getQuery('offset') ?: 0;
    }

    public function setLimit()
    {
        $this->arResult['LIMIT'] = $this->request->getQuery('limit') ?: self::DEFAULT_LIMIT;
    }

    public function setCities()
    {
        $this->arResult['CITIES'] = CityHelper::getList();

        foreach ($this->arResult['CITIES'] as $key => $city) {
            $uri = new Uri($this->request->getRequestUri());
            $uri->deleteParams(['filter']);
            $uri->addParams(['city' => $city['CODE']]);

            $word = Russian\GeographicalNamesInflection::getCase($city['NAME'], 'предложный');

            $this->arResult['CITIES'][$key]['NAME'] = 'в ' . str_replace(' И ', ' и ', $word);
            $this->arResult['CITIES'][$key]['LINK'] = $uri->getUri();
        }

        $uri = new Uri($this->request->getRequestUri());
        $uri->deleteParams(['filter']);
        $uri->addParams(['city' => self::DEFAULT_CITY_CODE]);

        $this->arResult['CITIES'] = array_merge(
            [
                self::DEFAULT_CITY_CODE => [
                    'CODE' => self::DEFAULT_CITY_CODE,
                    'NAME' => 'во всех городах',
                    'LINK' => $uri->getUri(),
                ],
            ],
            $this->arResult['CITIES']
        );
    }

    public function setCityProjects()
    {
        $query = Query::prepareQuery(Entities::PROJECT, [
            'select' => [
                'project' => [
                    'NAME' => 'UF_NAME',
                    'CODE' => 'UF_CODE',
                    'CITY' => 'UF_CITY',
                ],
            ],
        ]);

        if ($this->arResult['CITY'] !== self::DEFAULT_CITY_CODE) {
            $query->registerRuntimeField('IB_CITY', [
                'data_type' => Bitrix\Iblock\ElementTable::class,
                'reference' => [
                    '=this.UF_CITY'  => 'ref.ID',
                ],
            ]);

            $query->addFilter('IB_CITY.CODE', $this->arResult['CITY']);
        }

        $this->arResult['CITY_PROJECTS'] = $query->exec()->fetchAll();

        foreach ($this->arResult['CITY_PROJECTS'] as $index => $project) {
            $this->arResult['CITY_PROJECTS'][$index]['CHECKED'] = in_array($project['CODE'], (array) $this->arResult['FILTER']['PROJECT.UF_CODE']);
        }
    }

    public function setFlatsFilters()
    {
        $this->setCheckboxFilter('PLAN.UF_TYPE');
        $this->setCheckboxFilter('UF_POSITIVE_OUTSIDE');
        $this->setCheckboxFilter('PLAN.UF_RESIDENTS');

        $this->setRangeFilter('UF_TOTAL_PRICE');
        $this->setRangeFilter('PLAN.UF_TOTAL_AREA');
        $this->setRangeFilter('FLOOR.UF_NUMBER');

        $this->setYearRangeFilter('PROJECT.UF_COMPLETION', 'Y');

        $this->setMaxFilter('FLOOR.UF_NUMBER');

        $this->setCheckboxFilterParkingAndPantry();
    }

    public function prepareFieldAliase($field)
    {
        return str_replace('.', '_', str_replace('UF_', '', $field));
    }

    public function setCheckboxFilter($field)
    {
        $this->arResult['FLATS_FILTERS'][$field] = [];

        $query = $this->prepareFlatQuery();

        $fieldAliase = $this->prepareFieldAliase($field);

        $query->setSelect([
            $fieldAliase => $field,
        ]);

        $query->setGroup([
            $fieldAliase,
        ]);

        $items = $query->exec()->fetchALl();

        $values = $this->getUniqueValues($items, $fieldAliase);

        if (empty($values)) {
            return;
        }

        $enumFieldEntity = Entities::FLAT;
        $enumField = $field;
        if (strpos($field, '.') !== false) {
            $enumFieldEntity = strtolower(stristr($field, '.', true));
            $enumField = str_replace('.', '', stristr($field, '.'));
        }

        $enumFieldValues = Helpers::getEnumFieldValues($enumFieldEntity, $enumField);

        foreach ($values as $propertyId) {
            $this->arResult['FLATS_FILTERS'][$field][] = [
                'NAME' => $enumFieldValues[$propertyId]['NAME'],
                'VALUE' => $propertyId,
                'CHECKED' => (bool) in_array($propertyId, $this->arResult['FILTER'][$field]),
            ];
        }
    }

    public function setCheckboxFilterParkingAndPantry()
    {
        $this->arResult['FLATS_FILTERS']['PARKING_AND_PANTRY'] = [];

        $query = Query::prepareQuery(Entities::PROJECT);

        $query->registerRuntimeField('PARKING_COUNT', [
            'data_type' => 'integer',
            'expression' => ['IFNULL(%s, 0) + IFNULL(%s, 0)', 'UF_PARKING_BUY', 'UF_PARKING_RENT'],
        ]);

        $query->registerRuntimeField('PANTRY_COUNT', [
            'data_type' => 'integer',
            'expression' => ['IFNULL(%s, 0) + IFNULL(%s, 0)', 'UF_PANTRY_BUY', 'UF_PANTRY_RENT'],
        ]);

        $query->setSelect([
            'SUM_PARKING_COUNT' => BitrixQuery::expr()->sum('PARKING_COUNT'),
            'SUM_PANTRY_COUNT' => BitrixQuery::expr()->sum('PANTRY_COUNT'),
        ]);

        $result = $query->exec()->fetch();

        if (empty($result)) {
            return;
        }

        if (!empty($result['SUM_PARKING_COUNT'])) {
            $checked = false;
            array_walk_recursive($this->arResult['FILTER'], function($value, $key) use(&$checked) {
                if ($key === '>PROJECT.UF_PARKING_BUY') {
                    $checked = true;
                    return;
                }
            });

            $this->arResult['FLATS_FILTERS']['PARKING_AND_PANTRY']['SUM_PARKING_COUNT'] = [
                'VALUE' => 'Y',
                'NAME' => 'Машиноместа',
                'CHECKED' => $checked,
            ];
        }

        if (!empty($result['SUM_PANTRY_COUNT'])) {
            $checked = false;
            array_walk_recursive($this->arResult['FILTER'], function($value, $key) use(&$checked) {
                if ($key === '>PROJECT.UF_PANTRY_BUY') {
                    $checked = true;
                    return;
                }
            });

            $this->arResult['FLATS_FILTERS']['PARKING_AND_PANTRY']['SUM_PANTRY_COUNT'] = [
                'VALUE' => 'Y',
                'NAME' => 'Кладовой',
                'CHECKED' => $checked,
            ];
        }
    }

    public function setRangeFilter($field)
    {
        $this->arResult['FLATS_FILTERS'][$field] = [];

        $query = $this->prepareFlatQuery();

        $query->setSelect([
            'MAX' => BitrixQuery::expr()->max($field),
            'MIN' => BitrixQuery::expr()->min($field),
        ]);

        $result = $query->exec()->fetch();

        if ($result['MIN'] === null || $result['MAX'] === null) {
            return;
        }

        $this->arResult['FLATS_FILTERS'][$field] = [
            'DEFAULT_MIN' => $this->arResult['FILTER']['><' . $field][0] ?: $result['MIN'],
            'MIN' => $result['MIN'],
            'DEFAULT_MAX' => $this->arResult['FILTER']['><' . $field][1] ?: $result['MAX'],
            'MAX' => $result['MAX'],
        ];
    }

    public function setYearRangeFilter($field, $format)
    {
        $this->arResult['FLATS_FILTERS'][$field] = [];

        $query = $this->prepareFlatQuery();

        $query->setSelect([
            'MAX' => BitrixQuery::expr()->max($field),
            'MIN' => BitrixQuery::expr()->min($field),
        ]);

        $result = $query->exec()->fetch();

        if ($result['MIN'] === null || $result['MAX'] === null) {
            return;
        }

        $filterMin = $this->arResult['FILTER']['><' . $field][0];
        $filterMax = $this->arResult['FILTER']['><' . $field][1];

        $this->arResult['FLATS_FILTERS'][$field] = [
            'DEFAULT_MIN' => $filterMin ? date('Y', strtotime($filterMin)) : $result['MIN'],
            'MIN' => $result['MIN']->format($format),
            'DEFAULT_MAX' => $filterMax ? date('Y', strtotime($filterMax)) : $result['MAX'],
            'MAX' => $result['MAX']->format($format),
        ];
    }

    public function getUniqueValues($items, $fieldName)
    {
        $values = [];
        foreach ($items as $item) {
            $values = array_merge($values, (array) $item[$fieldName]);
        }

        $values = array_unique(array_filter($values));
        sort($values);

        return $values ?: [];
    }

    public function setMaxFilter($field)
    {
        $query = $this->prepareFlatQuery();

        $filter = $query->getFilter();

        unset($filter['UF_ACTIVE']);

        $query->setFilter($filter);

        $query->setSelect([
            'MAX' => BitrixQuery::expr()->max($field),
        ]);

        $result = $query->exec()->fetch();

        $fieldAliase = $this->prepareFieldAliase($field);

        $this->arResult['FLATS_FILTERS'][$fieldAliase . '_MAX']['VALUE'] = $result['MAX'];
    }

    public function prepareFlatQuery($data = [])
    {
        $query = Query::prepareQuery(Entities::FLAT, [
            'select' => $data['select'] ?: [],
            'filter' => array_merge_recursive([
                'flat' => [
                    'UF_ACTIVE' => true,
                ],
            ], $data['filter'] ?: []),
            'referenceFields' => array_merge_recursive([
                'flat' => ['plan' => 'UF_PLAN'],
            ], $data['referenceFields'] ?: []),
            'group' => $data['group'] ?: [],
            'order' => $data['order'] ?: [],
        ]);

        if ($this->arResult['CITY'] !== self::DEFAULT_CITY_CODE) {
            $query->registerRuntimeField('CITY', [
                'data_type' => \Bitrix\Iblock\ElementTable::class,
                'reference' => [
                    '=this.PROJECT.UF_CITY'  => 'ref.ID',
                ],
            ]);

            $query->addFilter('CITY.CODE', $this->arResult['CITY']);
        }

        return $query;
    }

    public function setFlats()
    {
        $this->arResult['FLATS'] = [];

        $query = $this->prepareFlatQuery([
            'select' => [
                'project' => [
                    'UF_CODE',
                    'UF_NAME',
                ],
                'building' => [
                    'UF_NUMBER',
                ],
                'section' => [
                    'UF_NUMBER',
                ],
                'floor' => [
                    'UF_NUMBER',
                ],
                'flat' => [
                    'ID',
                    'TOTAL_PRICE' => 'UF_TOTAL_PRICE',
                ],
                'plan' => [
                    'UF_TYPE',
                    'UF_TOTAL_AREA',
                    'UF_IMAGE',
                    'UF_EMPTY',
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $query->setOrder($this->arResult['ORDER']);
        $query->setOffset($this->arResult['OFFSET']);
        $query->setLimit($this->arResult['LIMIT'] + 1);

        $flats = $query->exec()->fetchAll() ?: [];

        $this->arResult['CAN_MORE'] = count($flats) === $this->arResult['LIMIT'] + 1;

        if (empty($flats)) {
            return;
        }

        $planTypeEnumValues = Helpers::getEnumFieldValues('plan', 'UF_TYPE');

        $flats = array_slice($flats, 0, $this->arResult['LIMIT']);

        $favorites = unserialize($this->request->getCookie('favorites_flat'));

        foreach ($flats as $flat) {
            $this->arResult['FLATS'][] = [
                'ID' => (int) $flat['ID'],
                'DETAIL_URL' => str_replace(['#PROJECT_CODE#', '#ID#'], [$flat['PROJECT_CODE'], $flat['ID']], '/projects/#PROJECT_CODE#/flat/#ID#/'),
                'IMAGE' => PlanHelper::getPlanImage($flat['PLAN_IMAGE'], !empty($flat['PLAN_EMPTY']), ['width' => 72, 'height' => 53]),
                'TYPE' => (string) $planTypeEnumValues[$flat['PLAN_TYPE']]['NAME'],
                'TOTAL_AREA' => number_format((double) $flat['PLAN_TOTAL_AREA'], 2, ',', ''),
                'SECTION' => implode('/', [$flat['BUILDING_NUMBER'], $flat['SECTION_NUMBER']]),
                'FLOOR' => (int) $flat['FLOOR_NUMBER'],
                'TOTAL_PRICE' => number_format((int) $flat['TOTAL_PRICE'], 0, '', ' '),
                'PROJECT' => $flat['PROJECT_NAME'],
                'IS_FAVORITE' => in_array($flat['ID'], $favorites ?: []),
            ];
        }
    }

    public function setPlans()
    {
        $this->arResult['PLANS'] = [];

        $query = $this->prepareFlatQuery([
            'select' => [
                'flat' => [
                    'PLAN_ID' => 'UF_PLAN',
                    'FLAT_TOTAL_PRICE_MIN' => BitrixQuery::expr()->min('UF_TOTAL_PRICE'),
                ],
                'plan' => [
                    'ID',
                    'UF_TYPE',
                    'UF_IMAGE',
                    'UF_TOTAL_AREA',
                    'UF_CHARACTERISCTIC',
                    'UF_EMPTY',
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
            'group' => [
                'flat' => [
                    'UF_PLAN',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $query->setOrder($this->arResult['ORDER']);
        $query->setOffset($this->arResult['OFFSET']);
        $query->setLimit($this->arResult['LIMIT'] + 1);

        $plans = $query->exec()->fetchAll() ?: [];

        $this->arResult['CAN_MORE'] = count($plans) === $this->arResult['LIMIT'] + 1;

        if (empty($plans)) {
            return;
        }

        $this->setPlansProjects();

        $planTypeEnumValues = Helpers::getEnumFieldValues('plan', 'UF_TYPE');

        $plans = array_slice($plans, 0, $this->arResult['LIMIT']);

        $favorites = unserialize($this->request->getCookie('favorites_plan'));

        foreach ($plans as $plan) {
            $planProjects = $this->arResult['PLANS_PROJECTS'][$plan['PLAN_ID']] ?: [];

            $moreProjectsCount = count($planProjects) - 3;
            $moreProjectsFlatsText = '';

            if ($moreProjectsCount >= 1) {
                $moreProjects = array_slice($planProjects, 3);
                $moreFlatsCount = 0;
                foreach ($moreProjects as $project) {
                    $moreFlatsCount += $project['FLATS_COUNT'];
                }

                if ($moreFlatsCount) {
                    $moreProjectsFlatsText = implode(' ', [
                        'И ещё',
                        Russian\pluralize($moreFlatsCount, 'квартира'),
                        'в',
                        Russian\CardinalNumeralGenerator::getCase($moreProjectsCount, 'предложный'),
                        'ЖК',
                    ]);
                }
            }
            $this->arResult['PLANS'][] = [
                'ID' => (int) $plan['PLAN_ID'],
                'IMAGE' => PlanHelper::getPlanImage($plan['PLAN_IMAGE'], !empty($plan['PLAN_EMPTY']), ['width' => 224, 'height' => 224]),
                'TYPE' => self::PLAN_TYPES[$planTypeEnumValues[$plan['PLAN_TYPE']]['XML_ID']] ?: $planTypeEnumValues[$plan['PLAN_TYPE']]['NAME'],
                'AREA' => number_format((double) $plan['PLAN_TOTAL_AREA'], 2, ',', ''),
                'PRICE' => number_format((int) $plan['FLAT_TOTAL_PRICE_MIN'], 0, '', ' '),
                'CAPTION' => (string) $plan['PLAN_CHARACTERISCTIC'],
                'PROJECTS' => $planProjects,
                'MORE_TEXT' => $moreProjectsFlatsText,
                'IS_FAVORITE' => in_array($plan['PLAN_ID'], $favorites) ? true : false,
            ];
        }
    }

    public function setPlansProjects()
    {
        $this->arResult['PLANS_PROJECTS'] = [];

        $query = $this->prepareFlatQuery([
            'select' => [
                'project' => [
                    'UF_NAME',
                ],
                'flat' => [
                    'UF_PLAN',
                    'FLATS_COUNT' => BitrixQuery::expr()->count('ID'),
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
            'group' => [
                'project' => [
                    'UF_CODE',
                ],
            ],
            'order' => [
                'flat' => [
                    'FLATS_COUNT' => 'DESC',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $items = $query->exec()->fetchAll() ?: [];

        if (empty($items)) {
            return;
        }

        foreach ($items as $value) {
            $ufPlan = $value['UF_PLAN'];
            unset($value['UF_PLAN']);

            if (!empty($ufPlan)) {
                $this->arResult['PLANS_PROJECTS'][$ufPlan][] = $value;
            }
        }
    }

    public function setFlatsCount()
    {
        $query = $this->prepareFlatQuery([
            'select' => [
                'flat' => [
                    'FLATS_COUNT' => BitrixQuery::expr()->count('ID'),
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $this->arResult['FLATS_COUNT'] = (int) $query->exec()->fetch()['FLATS_COUNT'];
    }

    public function setPlansCount()
    {
        $query = $this->prepareFlatQuery([
            'select' => [
                'flat' => [
                    'PLANS_COUNT' => BitrixQuery::expr()->countDistinct('UF_PLAN'),
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $this->arResult['PLANS_COUNT'] = (int) $query->exec()->fetch()['PLANS_COUNT'];
    }

    public function processAjaxAction()
    {
        switch ($this->request->getQuery('ajaxAction')) {
            case 'getPlanFlats':
                $this->setPlanProject();
                $this->setPlanFlats();
                $this->setPlanFlatsCount();
                $this->setPlanFlatsCountPluralize();
                $this->setPlanImage();

                break;
        }
    }

    public function setPlanProject()
    {
        $query = $this->prepareFlatQuery([
            'select' => [
                'project' => [
                    'UF_CODE',
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $this->arResult['PLAN_PROJECT'] = $query->exec()->fetch();
    }

    public function setPlanFlats()
    {
        $this->arResult['PLAN_FLATS'] = [];

        $query = $this->prepareFlatQuery([
            'select' => [
                'project' => [
                    'UF_CODE',
                    'UF_NAME',
                ],
                'building' => [
                    'UF_NUMBER',
                ],
                'section' => [
                    'UF_NUMBER',
                ],
                'floor' => [
                    'UF_NUMBER',
                ],
                'flat' => [
                    'ID',
                    'TOTAL_PRICE' => 'UF_TOTAL_PRICE',
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $query->setOrder(['UF_TOTAL_PRICE']);

        $flats = $query->exec()->fetchAll() ?: [];

        if (empty($flats)) {
            return;
        }

        $favorites = unserialize($this->request->getCookie('favorites_flat'));

        foreach ($flats as $flat) {
            $this->arResult['PLAN_FLATS'][] = [
                'ID' => (int) $flat['ID'],
                'DETAIL_URL' => str_replace(['#PROJECT_CODE#', '#ID#'], [$flat['PROJECT_CODE'], $flat['ID']], '/projects/#PROJECT_CODE#/flat/#ID#/'),
                'PROJECT' => $flat['PROJECT_NAME'],
                'SECTION' => implode('/', [$flat['BUILDING_NUMBER'], $flat['SECTION_NUMBER']]),
                'FLOOR' => (int) $flat['FLOOR_NUMBER'],
                'TOTAL_PRICE' => number_format((int) $flat['TOTAL_PRICE'], 0, '', ' '),
                'RAW_TOTAL_PRICE' => $flat['TOTAL_PRICE'],
                'IS_FAVORITE' => in_array($flat['ID'], $favorites ?: []),
            ];
        }
    }

    public function setPlanFlatsCount()
    {
        $query = $this->prepareFlatQuery([
            'select' => [
                'flat' => [
                    'PLAN_FLATS_COUNT' => BitrixQuery::expr()->count('ID'),
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $this->arResult['PLAN_FLATS_COUNT'] = (int) $query->exec()->fetch()['PLAN_FLATS_COUNT'];
    }

    public function setPlanFlatsCountPluralize()
    {
        $this->arResult['PLAN_FLATS_COUNT_PLURALIZE'] = Pluralize::getByType($this->arResult['PLAN_FLATS_COUNT'], 'project_live_types', $this->arResult['PLAN_PROJECT']);
    }

    public function setPlanImage()
    {
        $query = $this->prepareFlatQuery([
            'select' => [
                'plan' => [
                    'PLAN_IMAGE' => 'UF_IMAGE',
                ],
            ],
            'group' => [
                'flat' => [
                    'UF_PLAN',
                ],
            ],
            'referenceFields' => [
                'flat' => [
                    'flat' => 'ID',
                ],
            ],
        ]);

        $query->setFilter(array_merge(
            $query->getFilter() ?: [],
            $this->arResult['FILTER'] ?: []
        ));

        $result = $query->exec()->fetch();

        $this->arResult['PLAN_IMAGE'] = CFile::ResizeImageGet($result['PLAN_IMAGE'], ['width' => 550, 'height' => 550]);
    }

    public function getAjaxData()
    {
        if ($this->request->getQuery('ajaxAction') === 'getPlanFlats') {
            return [
                'flats' => $this->arResult['PLAN_FLATS'],
                'planImage' => $this->arResult['PLAN_IMAGE'],
                'countPluralize' => $this->arResult['PLAN_FLATS_COUNT_PLURALIZE'] . ' в этой планировке',
            ];
        }

        if ($this->arResult['VIEW'] === 'list') {
            return [
                'flats' => $this->arResult['FLATS'],
                'count' => $this->arResult['FLATS_COUNT'],
                'countPluralize' => $this->getFlatsPluralizePhrase($this->arResult['FLATS_COUNT']),
                'switchCount' => $this->arResult['PLANS_COUNT'],
                'canMore' => $this->arResult['CAN_MORE'],
                'filter' => $this->arResult['FILTER'],
            ];
        } elseif ($this->arResult['VIEW'] === 'plan') {
            return [
                'plans' => $this->arResult['PLANS'],
                'count' => $this->arResult['PLANS_COUNT'],
                'countPluralize' => $this->getCountPluralize($this->arResult['PLANS_COUNT'], 'plan'),
                'switchCount' => $this->arResult['FLATS_COUNT'],
                'canMore' => $this->arResult['CAN_MORE'],
                'filter' => $this->arResult['FILTER'],
            ];
        }
    }

    public function getCountPluralize($count, $type)
    {
        return Pluralize::getByType($count, $type);
    }

    public function setProjectsLiveType()
    {
        $this->arResult['PROJECT_LIVE_TYPE'] = ProjectHelper::defineDominantLiveType(array_column($this->arResult['CITY_PROJECTS'], 'CODE'));
    }

    public function getFlatsPluralizePhrase($number)
    {
        switch ($this->arResult['PROJECT_LIVE_TYPE']) {
            case ProjectHelper::FLAT_APARTMENT_TYPE:
                return Pluralize::getByType($number, 'variant');
                break;
            default:
                return Pluralize::getByType($number, $this->arResult['PROJECT_LIVE_TYPE']);
        }
    }

}
