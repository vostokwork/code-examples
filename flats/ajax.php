<?php

define('NO_KEEP_STATISTIC', true);
define('NOT_CHECK_PERMISSIONS', true);
define('BX_BUFFER_USED', true);

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_before.php';

use Bitrix\Main\Loader;

class ComponentAjax
{
    public function handle()
    {
        if (!Loader::includeModule('ds.development_layout')) {
            $this->flush([
                'result' => 'error',
                'text' => 'Не подключен модуль ds.development_layout',
            ]);

            return;
        }

        if (!Loader::includeModule('iblock')) {
            $this->flush([
                'result' => 'error',
                'text' => 'Не подключен модуль iblock',
            ]);

            return;
        }

        $mainComponentClass = \CBitrixComponent::includeComponentClass('project:flats');
        $mainComponent = new $mainComponentClass;
        $mainComponent->initComponent('project:flats');

        $mainComponent->processLogic();

        $this->flush([
            'result' => 'success',
            'data' => $mainComponent->getAjaxData(),
        ]);
    }

    public function flush(array $data)
    {
        $GLOBALS['APPLICATION']->RestartBuffer();
        echo \Bitrix\Main\Web\Json::encode($data);
        die();
    }
}

$componentAjax = new ComponentAjax();
$componentAjax->handle();
