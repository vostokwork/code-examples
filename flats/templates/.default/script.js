function flatsComponent(params) {
    var self = this;

    self.initParams(params);
    self.initElements();
    self.addEventListners();
}

flatsComponent.prototype.initParams = function (params) {
    var self = this;

    for (var paramName in params) {
        if (params.hasOwnProperty(paramName)) {
            self[paramName] = params[paramName];
        }
    }

    self.changeFormTimeout = undefined;

    self.currentOffset = self.offset;
}

flatsComponent.prototype.initElements = function (params) {
    var self = this;

    self.$sortSelect = $('.js-sortSelect');

    self.$switchLink = $('.js-switch-link');
    self.$switchCount = $('.js-switchCount');

    self.$form = $('.page-filter__form .js-modalApartmentForm');
    self.$countPluralize = $('.js-count-pluralize');
    self.$buttonFilterSubmit = $('.js-button-filter-submit');
    self.$buttonFilterEmpty = $('.js-button-filter-empty');

    self.$pageTable = $('.js-page-table');

    self.$sortHeaders = $('.sort-header[data-order-field]');
    self.$flatsTableBody = $('.apartment-table table tbody');

    self.$plansTable = $('.js-plans-table');
    self.$modalCard = $('.js-modalCard');
    self.$modalCardTable = $('.js-modalCard .apartment-table tbody');
    self.$modalCardSort = $('.js-modal-card-sort');

    self.$loadMoreButton = $('.js-load-more');
    self.$noResult = $('.js-no-result');
}

flatsComponent.prototype.addEventListners = function () {
    var self = this;

    self.$form.on('filterChange', self.onChangeForm.bind(self));
    self.$loadMoreButton.on('click', self.onClickLoadMoreButton.bind(self));
    self.$sortSelect.on('change', self.onChangeSortSelect.bind(self));
    self.$sortHeaders.on('click', self.onClickSortHeader.bind(self));
    self.$plansTable.on('click', '.js-modalCardlLink', self.onClickModalCardlLink.bind(self));
    self.$modalCardSort.on('click', self.onClickModalCartSort.bind(self));
}

flatsComponent.prototype.onChangeForm = function () {
    var self = this;

    self.deleteChangeFormTimeout();
    self.setChangeFormTimeout();
}

flatsComponent.prototype.deleteChangeFormTimeout = function () {
    var self = this;

    clearTimeout(self.changeFormTimeout);
}

flatsComponent.prototype.setChangeFormTimeout = function () {
    var self = this;

    self.changeFormTimeout = setTimeout(self.changeForm.bind(self), 200);
}

flatsComponent.prototype.changeForm = function () {
    var self = this;

    self.fetch({
        offset: 0,
        limit: self.currentOffset + self.limit,
    });
}

flatsComponent.prototype.fetch = function (data) {
    var self = this;

    $.get(self.ajaxPath, self.getAjaxParams(data), self.onSuccessFetch.bind(self, data));
}

flatsComponent.prototype.getAjaxParams = function (data) {
    var self = this;

    data = Object(data);

    return $.merge(
        self.$form.serializeArray(),
        [
            {
                name: 'view',
                value: self.view,
            },
            {
                name: 'city',
                value: self.city,
            },
            {
                name: 'order[' + self.getOrderField() + ']',
                value: self.getOrderValue(),
            },
            {
                name: 'offset',
                value: data.offset || self.offset,
            },
            {
                name: 'limit',
                value: data.limit || self.limit,
            },
        ]
    );
}

flatsComponent.prototype.onSuccessFetch = function (data, result) {
    var self = this;

    result = JSON.parse(result);

    if (result.result === 'success') {
        if (self.view === 'list') {
            self.renderFlatsList(result.data.flats, Number(data.offset) === 0);
            self.renderBanners(Number(data.offset) === 0, result.data.canMore);
        }

        if (self.view === 'plan') {
            self.renderPlansGrid(result.data.plans, Number(data.offset) === 0);
            self.renderBanners(Number(data.offset) === 0, result.data.canMore);
        }

        self.updateButtonFilterSubmit(result.data.count);
        self.updateCountPluralize(result.data.countPluralize);
        self.updateSwitchCount(result.data.switchCount);
        self.updateLinks(result.data.filter);
        self.updateWindowUrl(result.data.filter);
        self.updateResultView(result.data.count);
        self.updateMoreButton(result.data.canMore);
        self.fixLeakedCard(result.data.canMore);
    }
}

flatsComponent.prototype.updateResultView = function (count) {
    var self = this;

    self.$pageTable.toggleClass('hidden', !Boolean(count));
    self.$noResult.toggleClass('hidden', Boolean(count));
}

flatsComponent.prototype.updateMoreButton = function (canMore) {
    var self = this;

    self.$loadMoreButton.toggleClass('hidden', !canMore);
}

flatsComponent.prototype.onClickLoadMoreButton = function (event) {
    event.preventDefault();

    var self = this;

    self.currentOffset += self.limit;

    self.fetch({
        offset: self.currentOffset,
        limit: self.limit,
    });
}

flatsComponent.prototype.onChangeSortSelect = function (event, skipOnChange) {
    var self = this;

    if (skipOnChange) {
        return;
    }

    var orderField = self.$sortSelect.val();
    var orderDirection = 'ASC';

    var newOrder = {};
    newOrder[orderField] = orderDirection;

    self.setOrder(newOrder);

    self.updateSortView();
}

flatsComponent.prototype.setOrder = function (order) {
    var self = this;

    self.order = order;
}

flatsComponent.prototype.getOrderField = function () {
    var self = this;

    for (field in self.order) {
        return field;
    }
}

flatsComponent.prototype.getOrderValue = function () {
    var self = this;

    for (field in self.order) {
        return self.order[field];
    }
}

flatsComponent.prototype.updateCountPluralize = function (title) {
    var self = this;

    self.$countPluralize.text(title);
}

flatsComponent.prototype.updateSwitchCount = function (count) {
    var self = this;

    self.$switchCount.text(count)
}

flatsComponent.prototype.updateLinks = function (filter) {
    var self = this;

    self.replaceUrlParamsOnElement(self.$switchLink, filter);
}

flatsComponent.prototype.replaceUrlParamsOnElement = function ($element, filter) {
    var self = this;

    $element.each(function() {
        $this = $(this);

        href = $this.attr('href');
        href = href.substring(0, href.indexOf('?') === -1 ? href.length : href.indexOf('?'));

        $this.attr('href', href + '?' + $.param({filter: filter}));
    });
}

flatsComponent.prototype.updateWindowUrl = function (filter) {
    var self = this;

    if (typeof history.replaceState !== 'function') {
        return;
    }

    href = window.location.href;
    href = href.substring(0, href.indexOf('?') === -1 ? href.length : href.indexOf('?'));

    history.replaceState(undefined, undefined, href + '?' + $.param({filter: filter}));
}

flatsComponent.prototype.onClickSortHeader = function (event) {
    var self = this;

    var $clickedSortHeader = $(event.currentTarget);
    var $prevSortHeader = self.$sortHeaders.not('.order-hidden');

    var orderField = $clickedSortHeader.attr('data-order-field');
    var orderDirection = $clickedSortHeader.attr('data-order-direction');

    if ($clickedSortHeader.is($prevSortHeader)) {
        orderDirection = orderDirection === 'ASC' ? 'DESC' : 'ASC';
    }

    var newOrder = {};
    newOrder[orderField] = orderDirection;

    self.setOrder(newOrder);

    self.updateSortView();
}

flatsComponent.prototype.updateSortView = function () {
    var self = this;

    // Обновляем заголовки сортировки в таблице
    self.$sortHeaders.attr('data-order-direction', 'ASC').removeClass('sort-down').addClass('sort-up').addClass('order-hidden');

    var $currentSortHeader = self.$sortHeaders.filter('[data-order-field="' + self.getOrderField() + '"]');

    $currentSortHeader.attr('data-order-direction', self.getOrderValue());
    $currentSortHeader.toggleClass('sort-up', self.getOrderValue() === 'ASC').toggleClass('sort-down', self.getOrderValue() !== 'ASC');
    $currentSortHeader.removeClass('order-hidden');

    // Обновляем селект
    self.$sortSelect.val(self.getOrderField());
    self.$sortSelect.trigger('change', true);

    self.fetch({
        offset: 0,
        limit: self.currentOffset + self.limit,
    });
}

flatsComponent.prototype.renderFlatsList = function (flats, isClearTable) {
    var self = this;

    var scrollTop = $(document).scrollTop();

    if (isClearTable) {
        self.$flatsTableBody.empty();
    }

    $(flats).each(function (index, flat) {
        self.$flatsTableBody.append($(''
            + '<tr class="js-link" data-url="' + flat['DETAIL_URL'] + '">'
                + '<td><img src="' + flat['IMAGE']['src'] + '" width="' + flat['IMAGE']['width'] + '" height="' + flat['IMAGE']['height'] + '" alt=""></td>'
                + '<td>' + flat['TYPE'] + '</td>'
                + '<td>' + flat['TOTAL_AREA'] + '</td>'
                + '<td>' + flat['PROJECT'] + '</td>'
                + '<td>' + flat['SECTION'] + '</td>'
                + '<td>' + flat['FLOOR'] + '</td>'
                + '<td>' + flat['TOTAL_PRICE'] + '</td>'
                + '<td>'
                    + '<a class="favorite-button js-favorite-add ' + (flat['IS_FAVORITE'] ? 'in_favourite' : '')  + '" href="javascript:;"'
                       + 'data-id="' + flat['ID'] + '" data-type="flat">'
                        + '<svg class="icon icon-fav ">'
                            + '<use xlink:href="' + self.markupPath + '/images/sprites.svg#fav"></use>'
                        + '</svg>'
                    + '</a>'
                + '</td>'
            + '</tr>'
        ));
    });

    window.scroll(0, scrollTop);
}

flatsComponent.prototype.renderPlansGrid = function (plans, isClearTable) {
    var self = this;



    if (isClearTable) {
        self.$plansTable.children().not(self.$modalCard).remove();
    }

    $(plans).each(function (index, plan) {
        var projectsHtml = '';
        for (var index = 0; index <= 2; index++) {
            if (!plan['PROJECTS'][index]) {
                break;
            }

            projectsHtml += ''
                + '<div class="plan-card__elem">'
                    + '<div class="plan-card__elem-item">ЖК «' + plan['PROJECTS'][index]['PROJECT_NAME'] + '»</div>'
                    + '<div class="plan-card__elem-numbers">' + plan['PROJECTS'][index]['FLATS_COUNT'] + '</div>'
                + '</div>';
        }

        $(''
            + '<div class="project-grid__col js-modalHeight" data-plan-id="' + plan['ID'] + '">'
                + '<div class="plan-card">'
                    + '<div data-id="' + plan['ID'] + '" data-type="plan" class="plan-card__icon js-favorite-add ' + (plan['IS_FAVORITE'] ? 'in_favourite' : '') + '">'
                        + '<svg class="icon icon-fav "><use xlink:href="' + self.markupPath + '/images/sprites.svg#fav"></use></svg>'
                    + '</div>'
                    + '<a class="plan-card__image js-modalCardlLink" href="javascript:;" data-modal-src="#modalCard">'
                        + '<img src="' + plan['IMAGE']['src'] + '" alt="Картинка планировки">'
                    + '</a>'
                    + '<div class="plan-card__description">'
                        + '<div class="plan-card__subtitle">' + (plan['CAPTION'] ? plan['CAPTION'] : '&nbsp;') + '</div>'
                        + '<a class="plan-card__title js-modalCardlLink" href="javascript:;" data-modal-src="#modalCard">' + plan['TYPE'] + '</a>'
                        + '<div class="plan-card__area">' + plan['AREA'] + ' м<sup>2</sup></div>'
                    + '</div>'
                    + '<div class="plan-card__list">'
                        + projectsHtml
                    + '</div>'
                    + '<div class="plan-card__text">' + plan['MORE_TEXT'] + '</div>'
                    + '<div class="plan-card__price">от ' + plan['PRICE'] + ' ₽</div>'
                + '</div>'
            + '</div>'
        ).insertBefore(self.$modalCard);
    });
}

flatsComponent.prototype.onClickModalCardlLink = function (event) {
    var self = this;

    var $planCard = $(event.currentTarget).closest('.project-grid__col');

    self.updateModalCard($planCard);
    self.fetchPlanFlats($planCard.data('plan-id'));
}

flatsComponent.prototype.updateModalCard = function ($planCard) {
    var self = this;

    var planTypeName = $planCard.find('.plan-card__title').text();
    var planArea = $planCard.find('.plan-card__area').html();

    self.$modalCard.find('.modal-card__title').text(planTypeName);
    self.$modalCard.find('.modal-card__subtitle').html(planArea);
    self.$modalCardSort.removeClass('sort-down').addClass('sort-up');
}

flatsComponent.prototype.fetchPlanFlats = function (planId) {
    var self = this;

    $.get(
        self.ajaxPath,
        $.merge(
            self.$form.serializeArray(),
            [
                {
                    name: 'ajaxAction',
                    value: 'getPlanFlats',
                },
                {
                    name: 'city',
                    value: self.city,
                },
                {
                    name: 'filter[PLAN.ID]',
                    value: planId,
                },
            ]
        ),
        self.onSuccessFetchPlanFlats.bind(self)
    );
}

flatsComponent.prototype.onSuccessFetchPlanFlats = function (result) {
    var self = this;

    result = JSON.parse(result);

    if (result.result === 'success') {
        self.updateModalCardTotalCount(result.data.countPluralize);
        self.updateModalCardPlanImage(result.data.planImage);
        self.renderPlanFlatsTable(result.data.flats);
    }
}

flatsComponent.prototype.updateModalCardTotalCount = function (countPluralize) {
    var self = this;

    self.$modalCard.find('.modal-card__content-text').text(countPluralize);
}

flatsComponent.prototype.updateModalCardPlanImage = function (planImage) {
    var self = this;

    self.$modalCard.find('.modal-card__image').empty();

    if (!planImage) {
        return;
    }

    self.$modalCard.find('.modal-card__image').html($('<img>', {'src': planImage.src}));
}

flatsComponent.prototype.renderPlanFlatsTable = function (flats) {
    var self = this;

    self.$modalCardTable.empty();

    $(flats).each(function (index, flat) {
        var $row = $(''
            + '<tr data-total-price="' + flat['RAW_TOTAL_PRICE'] + '" data-detail-url="' + flat['DETAIL_URL'] + '">'
                + '<td>'
                    + '<div class="mobile-label">ЖК</div><span>' + flat['PROJECT'] + '</span>'
                + '</td>'
                + '<td>'
                    + '<div class="mobile-label">Корпус/Секция</div><span>' + flat['SECTION'] + '</span>'
                + '</td>'
                + '<td>'
                    + '<div class="mobile-label">Этаж</div><span>' + flat['FLOOR'] + '</span>'
                + '</td>'
                + '<td>'
                    + '<div class="mobile-label">Стоимость,</div>'
                    + '<span>' + flat['TOTAL_PRICE'] + ' <svg class="icon icon-ruble "><use xlink:href="' + self.markupPath + '/images/sprites.svg#ruble"></use></svg></span>'
                + '</td>'
                + '<td>'
                    + '<a data-id="' + flat['ID'] + '" data-type="flat" class="favorite-button js-favorite-add ' + (flat['IS_FAVORITE'] ? 'in_favourite' : '')  + '" href="javascript:;">'
                        + '<svg class="icon icon-fav "><use xlink:href="' + self.markupPath + '/images/sprites.svg#fav"></use></svg>'
                    + '</a>'
                + '</td>'
            + '</tr>'
        );

        $row.on('click', self.onClickModalFlatRow.bind(self));

        self.$modalCardTable.append($row);
    });
}

flatsComponent.prototype.onClickModalFlatRow = function (event) {
    event.preventDefault();

    if ($(event.target).prop('tagName') === 'A') {
        return;
    }

    window.location = $(event.currentTarget).attr('data-detail-url');
}

flatsComponent.prototype.onClickModalCartSort = function (event) {
    var self = this;

    self.$modalCardSort.toggleClass('sort-down').toggleClass('sort-up');

    var orderDirection = self.$modalCardSort.hasClass('sort-up') ? 'ASC' : 'DESC';

    var flatsRows = self.$modalCardTable.find('tr');

    flatsRows.sort(function (a, b) {
        if (orderDirection === 'ASC') {
            return parseInt($(a).data('total-price')) - parseInt($(b).data('total-price'));
        } else {
            return parseInt($(b).data('total-price')) - parseInt($(a).data('total-price'));
        }
    });

    flatsRows.appendTo(self.$modalCardTable);
}

flatsComponent.prototype.updateButtonFilterSubmit = function (flatsCount) {
    var self = this;

    self.$buttonFilterSubmit.toggleClass('hidden', !Boolean(flatsCount));
    self.$buttonFilterEmpty.toggleClass('hidden', Boolean(flatsCount));
}

flatsComponent.prototype.renderBanners = function (fullRedraw, hasMore) {
    var manager = window.bannerManager;
    if(typeof manager === 'undefined') {
        return;
    }

    if(fullRedraw) {
        manager.markPositionalAsDisposed();
    }

    manager.setAllowedToPlaceLast(!hasMore);
    manager.setupBanners();
};

flatsComponent.prototype.fixLeakedCard = function(hasMore) {
    // if were working in 'plan' view we should make sure that banners won't push
    // one element onto the new page. Since we can't really prevent it from happening
    // on the backend side, we will hide elements which won't fit. Once new page is
    // loaded we will show them as part of that page and check if any elements 'leaking'
    // again, if so - hide them. This will continue until we load the final page.

    var self = this;

    if(this.view !== 'plan') {
        return;
    }

    if(typeof hasMore === 'undefined') {
        // if we have no idea about amount of pages, check how much elements we have - there
        // should be equal or more than elements per page
        var perPage = self.$plansTable.data('page-size');
        if(perPage) {
            // there must be data-page-size attribute in order for this to work
            var childCount = self.$plansTable.children(':not(.js-modalCard)');
            hasMore = (perPage <= childCount);
        }
    }

    var leakedCards = self.$plansTable.children('.js-banner-leaked');

    leakedCards.show();
    leakedCards.removeClass('js-banner-leaked');

    if(!hasMore) {
        return;
    }

    var planCards = self.$plansTable.children('.project-grid__col');
    var lastPosition = planCards.length - 1;

    // HARDCODED LINE WIDTH!
    var linePosition = lastPosition % 4;

    if(linePosition !== 3) {
        for(var i = lastPosition - linePosition; i < (lastPosition + 1); i++) {
            var card = $(planCards[i]);
            card.addClass('js-banner-leaked');
            card.hide();
        }
    }
};
