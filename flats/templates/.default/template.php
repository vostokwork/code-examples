<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) {
    die();
}
?>
<div class="page-filter page_bg_grey">
    <?php
    require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/page-header.php';
    ?>
    <?php
    require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/page-filter__form.php';
    ?>
    <div class="page-filter__content">
        <div class="container">
            <?php
            switch ($arParams['VIEW']) {
                case 'list':
                    require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/flats-table.php';
                    break;
                case 'plan':
                    require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/plans-table.php';
                    break;
            }
            ?>
        </div>
    </div>
</div>
<?php
require Bitrix\Main\Loader::getDocumentRoot() . $component->getPath() . '/includes/modal.php';
?>
