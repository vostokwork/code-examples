<script>
window.flatsComponentManager = new flatsComponent(<?=Bitrix\Main\Web\Json::encode([
    'view' => $this->arResult['VIEW'],
    'ajaxPath'=> $component->getPath() . '/ajax.php',
    'city' => $this->arResult['CITY'],
    'order' => $this->arResult['ORDER'],
    'offset' => $this->arResult['OFFSET'],
    'limit' => $this->arResult['LIMIT'],
    'markupPath' => MARKUP_PATH,
])?>);
</script>
